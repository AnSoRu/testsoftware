/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Angel
 */
@WebServlet(name = "NewServlet", urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
  
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException  {
        DBConnection db = new DBConnection();
        Connection conn = null;
        
        boolean flag=false;
        
        try {
            conn = db.getConnection();
            flag = true;
        } catch (Exception ex) {
            response.getWriter().println("<p>Error en el acceso a la base de datos.</p>");
        }

        if(flag){    
            flag=false;
            try { 
                double tasa = 1;
                double precio = 0;
                double descuento = 1;

                String items = request.getParameter("Items");
                String idIt = request.getParameter("IdItem");
                String st = request.getParameter("State");

                int itemsI = Integer.parseInt(items);

                String query = "SELECT Precio FROM price WHERE ID= "+idIt ;

                Statement st2 = null;
                st2 = conn.createStatement();
                ResultSet rs = null;
                rs = st2.executeQuery(query);
                while (rs.next())
                {
                     precio = rs.getDouble("Precio");
                     flag=true;
                }

                if(flag){
                    flag=false;

                    double pr = precio * itemsI;

                    query = "SELECT Descuento FROM discount WHERE Precio<= " + pr + " ORDER BY Precio";
                    st2 = conn.createStatement();
                    rs = st2.executeQuery(query);
                    while (rs.next())
                    {
                         descuento = 1 - rs.getFloat("Descuento");
                    }

                    query = "SELECT Tasa FROM taxes WHERE Estado = '" + st +"'";
                    st2 = conn.createStatement();
                    rs = st2.executeQuery(query);
                    while (rs.next())
                    {
                         tasa = 1 + rs.getFloat("Tasa");
                         flag=true;
                    }

                    if(flag){
                        double res = itemsI * tasa * precio * descuento;
                        response.setContentType("text/html;charset=UTF-8");
                        response.getWriter().println("Precio final: "+String.format("%.2f",res)+"$");

                    }else{
                        response.getWriter().println("<p>No se comercializan productos en este estado.</p>");
                    }
                }else{
                    response.getWriter().println("<p>No se ha encontrado el producto.</p>");
                }

            } catch (Exception ex) {
                response.getWriter().println("<p>Error en el proceso. Verifique los parámetros.</p>");
            }
        }

}

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

