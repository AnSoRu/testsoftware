package servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Angel
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

    private static final String HOST_NAME = "sql7.freesqldatabase.com";
    private static final String SERVER_PORT = "3306";
    private static final String DATABASE = "sql7150782";
    private static final String USERNAME = "sql7150782";
    private static final String PASSWORD = "7fAkTkASpr";

    public Connection getConnection() throws SQLException, ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", USERNAME);
        connectionProps.put("password", PASSWORD);

        conn = (Connection) DriverManager
                .getConnection("jdbc:mysql://" + HOST_NAME + ":" + SERVER_PORT + "/" + DATABASE, connectionProps);

        System.out.println("Connected to database");
        return conn;
    }
}
